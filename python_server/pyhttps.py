#!/usr/bin/env python3

# generate server.pem with the following command:
#    openssl req -new -x509 -keyout server.pem -out server.pem -days 365 -nodes

import argparse
import atexit
import logging
import os
import sys
import tempfile
from subprocess import call

import http.server
import socketserver
import ssl


from version import version

logging.basicConfig(level=logging.INFO)

PY3 = sys.version_info[0] == 3

class MyHTTPRequestHandler(http.server.SimpleHTTPRequestHandler):

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        logging.info('do GET SimpleHTTPRequestHandler')
        self._set_headers()
        self.wfile.write(str.encode('<html><body><h1>hi!</h1></body></html>'))

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        # Doesn't do anything with posted data
        logging.info('do POST SimpleHTTPRequestHandler')
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        logging.info(post_data)
        self._set_headers()
        # self.wfile.write(str.encode('<html><body><h1>POST!</h1></body></html>'))
        self.wfile.write(str.encode("<html><body><h1>POST!</h1><pre>") + post_data + str.encode("</pre></body></html>"))

parser = argparse.ArgumentParser(description='')
parser.add_argument('--host', dest='host', default='localhost')
parser.add_argument('--port', dest='port', type=int, default=4443)
args = parser.parse_args()

server_host = args.host
server_port = args.port
ssl_cert_path = './server.pem'

if PY3:
    OpenSslExecutableNotFoundError = FileNotFoundError
else:
    OpenSslExecutableNotFoundError = OSError


def create_ssl_cert():
    if PY3:
        from subprocess import DEVNULL
    else:
        DEVNULL = open(os.devnull, 'wb')

    try:
        ssl_exec_list = ['openssl', 'req', '-new', '-x509', '-keyout', ssl_cert_path,
                         '-out', ssl_cert_path, '-days', '365', '-nodes',
                         '-subj', '/CN=www.talhasch.com/O=Talhasch Inc./C=TR']
        call(ssl_exec_list, stdout=DEVNULL, stderr=DEVNULL)
    except OpenSslExecutableNotFoundError:
        logging.error('openssl executable not found!')
        exit(1)

    logging.info('Self signed ssl certificate created at {}'.format(ssl_cert_path))


def exit_handler():
    # remove certificate file at exit
    # os.remove(ssl_cert_path)

    logging.info('Bye!')

def main():
    logging.info('pyhttps {}'.format(version))
    # create_ssl_cert()
    atexit.register(exit_handler)

    logging.info('Server running... https://{}:{}'.format(server_host, server_port))
    logging.info('ssl_cert_path : {}'.format(ssl_cert_path))
    httpd = socketserver.TCPServer((server_host, server_port), MyHTTPRequestHandler)
    httpd.socket = ssl.wrap_socket(httpd.socket, certfile=ssl_cert_path, server_side=True)
    #
    # with letsenscrypt cert
    #
    # httpd.socket = ssl.wrap_socket(httpd.socket, certfile='./fullchain.pem', keyfile='./privkey.pem', server_side=True, ssl_version=ssl.PROTOCOL_TLSv1_2)
    #
    httpd.serve_forever()

if __name__ == '__main__':
    main()


