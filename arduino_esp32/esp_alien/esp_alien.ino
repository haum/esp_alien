#include <Adafruit_NeoPixel.h>
#include <ArduinoJson.h>
#include <ArduinoOTA.h>   //
#include <ESPmDNS.h>      //
#include <PubSubClient.h> // https://github.com/knolleary/pubsubclient
#include <WiFi.h>         //
#include <WiFiUdp.h>      //

#include "config.h" // see config.h.sample for details

// Define the array of leds
Adafruit_NeoPixel strip =
    Adafruit_NeoPixel(NUM_LEDS, DATA_PIN, NEO_GRB + NEO_KHZ800);

// MQTT
WiFiClient espClient;
PubSubClient client(espClient);

boolean space_open;
uint16_t rainbowCycle_step = 0;
long NoOneDelay = 0;

// Variables will change:
int buttonState;           // the current reading from the input pin
boolean lastButtonState = HIGH; // the previous reading from the input pin

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an
// int.
unsigned long lastDebounceTime = 0; // the last time the output pin was toggled
unsigned long debounceDelay =
    50; // the debounce time increase if the output flickers

void callback(char *topic, byte *payload, unsigned int length) {
  // Memory pool for JSON object tree.
  //
  // Inside the brackets, 400 is the size of the pool in bytes.
  // Don't forget to change this value to match your JSON document.
  // Use arduinojson.org/assistant to compute the capacity.

  StaticJsonBuffer<200> jsonBuffer;

  // handle message arrived

  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  JsonObject &root = jsonBuffer.parseObject(payload);

  if (!root.success()) {
    Serial.println("parseObject() failed");
    return;
  } else {
    const char *is_open = root["is_open"];
    Serial.println(is_open);
    space_open = (strcmp(is_open, "true") == 0);
    if (space_open) {
      colorWipe(strip.Color(0, 255, 0), 50); // Green
    } else {
      colorWipe(strip.Color(0, 0, 0), 50); // Green
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "Alien-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(), mqtt_user, mqtt_pass)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("spacestatus/answer");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  // put your setup code here, to run once:

  strip.begin();
  strip.show(); // Initialize all pixels to 'off'

  Serial.begin(115200);
  delay(1000);

  pinMode(PIR_IN, INPUT_PULLUP);
  pinMode(SW_IN, INPUT_PULLUP);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    spark_led();
    delay(100);
    Serial.println("Connecting to WiFi..");
  }

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  ArduinoOTA.setHostname("EspAlien");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using
    // SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() { Serial.println("\nEnd"); });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR)
      Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR)
      Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR)
      Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR)
      Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR)
      Serial.println("End Failed");
  });

  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Connected to the WiFi network");
  colorWipe(strip.Color(255, 0, 0), 50); // Red
  colorWipe(strip.Color(0, 255, 0), 50); // Green
  colorWipe(strip.Color(0, 0, 255), 50); // Blue
  colorWipe(strip.Color(0, 0, 0), 50);   // Black

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {

  // put your main code here, to run repeatedly:
  if (!client.connected()) {
    reconnect();
  }
  ArduinoOTA.handle();
  client.loop();

  // read the state of the switch into a local variable:
  boolean reading = digitalRead(SW_IN);

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH), and you've waited long enough
  // since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if (reading != lastButtonState) {
    Serial.println("button pressed");
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      // only toggle the satus if the new button state is LOW
      if (buttonState == LOW) {
        Serial.println("State changed");
        change_status();
      }
    }

  }
  lastButtonState = reading;

  if (space_open == false) {
    if (digitalRead(PIR_IN)) {
      // Serial.println("PIR_detect");
      rainbowCycle(5);
      NoOneDelay = 0;
    } else {
      NoOneDelay++;
      delay(20);
    }

    if (NoOneDelay > 10000) {
      // Serial.println("No One");
      colorWipe(strip.Color(0, 0, 0), 10); // black
      NoOneDelay = 10000;
    }
  }
}

void colorWipe(uint32_t c, uint8_t wait) {
  for (uint16_t i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}

void spark_led() {
  strip.setPixelColor(random(strip.numPixels()),
                      strip.Color(random(255), random(255), random(255)));
  strip.show();
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for (i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(
        i, Wheel(((i * 256 / strip.numPixels()) + rainbowCycle_step) & 255));
  }
  strip.show();
  delay(wait);
  if (rainbowCycle_step < 256 * 5) { // 5 cycles of all colors on wheel
    rainbowCycle_step++;
  } else
    rainbowCycle_step = 0;
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if (WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if (WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

void change_status() {
  if (space_open == false) {
    client.publish("spacestatus/query",
                   "{\"command\": \"open_silent\", \"source\": \"irc\"}");
  } else {
    client.publish("spacestatus/query",
                   "{\"command\": \"close_silent\", \"source\": \"irc\"}");
  }
}
